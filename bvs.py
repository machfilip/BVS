import urllib
import re
from bs4 import BeautifulSoup
from inspect import getsourcefile
import sys
import os
from urllib.parse import urlparse
import urllib.request
import mysql.connector
import yaml
from functions import getPrice

current_path = os.path.abspath(getsourcefile(lambda:0))
current_dir = os.path.dirname(current_path)
parent_dir = current_dir[:current_dir.rfind(os.path.sep)]

conf = yaml.load(open(parent_dir+'/conf/application.yml'))

url = 'http://www.bvs.cz/index2.html'
o = urlparse(url)

db_url = o.scheme+"://"+o.netloc+"/"

category = 'BVS Nove zbrane'

with urllib.request.urlopen(url) as response:
   html = response.read()

soup = BeautifulSoup(html, "html.parser")
l = soup.find_all("b")
i = 0

db_url_single = "";
db_title = "";
db_content = "";
db_price = "";

secret_user = conf['aws']['bvs']['username']
secret_password = conf['aws']['bvs']['password']
secret_database = conf['aws']['bvs']['database']
secret_host = conf['aws']['bvs']['host']
secret_port = conf['aws']['bvs']['port']

cnx = mysql.connector.connect(user=secret_user, password=secret_password, database=secret_database, host=secret_host, port=secret_port)
cursor = cnx.cursor()
query = "INSERT INTO bvs.Items (Item_URL, Item_Title, Item_Price) VALUES (%s, %s, %s)"

for inzerat in l:
	try:
		if(i%2 == 0):
			db_url_single = db_url+inzerat.a['href']
			db_title = inzerat.a.get_text()
			db_price = getPrice(db_url_single)

		i = i+1

	except:
		break

	query1 = "SELECT * FROM bvs.Items WHERE Item_URL = '"+db_url_single+"';"
	cursor.execute(query1)
	row = cursor.fetchall()

	if len(row) == 0:
		cursor.execute(query, ((db_url_single), (db_title), (db_price) ))
		cnx.commit()

cursor.close()
cnx.close()
