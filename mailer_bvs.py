from functions import sendMail
import mysql.connector
import sys
import yaml
from inspect import getsourcefile
import os.path
import sys

current_path = os.path.abspath(getsourcefile(lambda:0))
current_dir = os.path.dirname(current_path)
parent_dir = current_dir[:current_dir.rfind(os.path.sep)]


conf = yaml.load(open(parent_dir+'/conf/application.yml'))

secret_user = conf['aws']['bvs']['username']
secret_password = conf['aws']['bvs']['password']
secret_database = conf['aws']['bvs']['database']
secret_host = conf['aws']['bvs']['host']
secret_port = conf['aws']['bvs']['port']

cnx = mysql.connector.connect(user=secret_user, password=secret_password, database=secret_database, host=secret_host, port=secret_port)

cursor = cnx.cursor()
query = ("SELECT * FROM bvs.V_Unsent_Items ORDER BY Item_Category, Item_Title;")
cursor.execute(query)

rows = cursor.fetchall()
cursor.close()
cnx.close()

#print(len(rows) )

if len(rows) == 0:
	#print(len(rows))
	sys.exit()



message = "<html><body><table>"
for row in rows:
	#print(row[2] )
	link = "<tr><td><a href='"+row[0].decode("utf-8")+"'>"+row[1].decode("utf-8")+"</a></td><td>"+row[2].decode("utf-8")+"</td><td>"+row[3]+"</td></tr>"
	message = message + link
message = message+"</table></body></html>"

#print(message)

cnx = mysql.connector.connect(user=secret_user, password=secret_password, database=secret_database, host=secret_host, port=secret_port)
cursor = cnx.cursor()

i = sendMail(message)
if i == 1:
	query = ("UPDATE bvs.Items SET Send_Flag = 'Y' WHERE Send_Flag = 'N';")
	cursor.execute(query)
	cnx.commit()
	#print cursor._executed

cursor.close()
cnx.close()
